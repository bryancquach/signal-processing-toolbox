
import numpy as np
from scipy import stats

class Sketcher(object):

    """Apply sliding window random projection signal sketching.

    Provides functionality to transform a 2D signal into a bit-profile using
    a random projection.

    Attributes:
        _r: An array of randomly generated values from a Normal distribution.

        _w: An int for the sliding window size.

        _s: An int specifying the step size for the sliding window.
    
    Args:
        window_size: An int for the sliding window size.

        step_size: An int specifying the step size for the sliding window.

    Raises:
        ValueError: step size value not valid
    """

    def __init__(self, window_size, step_size):
        """Initialize Sketcher class."""
        self._w = window_size
        self._s = step_size
        self._r = stats.norm.rvs(loc=0, scale=1, size=self._w)

    def get_window_size(self):
        """Getter for sliding window size."""
        return self._w

    def get_step_size(self):
        """Getter for sliding window step size."""
        return self._s

    def get_projection_vector(self):
        """Getter for the random projection vector."""
        return self._r

    def set_window_size(self, w):
        """Setter for sliding window size.

        Args:
            w: An int for the new sliding window size.

        Returns:
            None

        Raises:
            ValueError: Invalid sliding window size specified.

            TypeError: Window size must be type 'int'.
        """
        if w <= 0: raise ValueError, "Invalid step size specified"
        if not isinstance(w, int): raise TypeError, \
            "Window size must be type 'int'"
        self._w = w
        return

    def set_step_size(self, s):
        """Setter for sliding window step size.

        Args:
            s: An int for the sliding window step size.

        Returns:
            None

        Raises:
            ValueError: Invalid step size specified.

            TypeError: Step size must be type 'int'.
        """
        if s <= 0: raise ValueError, "Invalid step size specified"
        if not isinstance(s, int): raise TypeError, \
            "Step size must be type 'int'"
        self._s = s
        return

    def set_projection_vector(self, r):
        """Setter for the projection vector.

        Args:
            r: An array-like vector with numeric values.

        Returns:
            None
        """
        self._r = np.asarray(r)
        return

    def segment(self, signal):
        """Partition a signal using a sliding window.

        Args:
            signal: An array-like with numeric values.
        
        Returns:
            A list containing the signal segments as arrays.
        """
        return [signal[i:i+self._w] for i in xrange(len(signal)-self._w+1)]

    def get_bit_profile(self, signal):
        """Calculate the bit-profile for given signal.

        Segments a given signal then uses a random projection on
        the segments to produce a dot product that is reduced to
        a sign vector.

        Args:
            signal: An array-like with numeric values.
        
        Returns:
            A bit-profile array.
        """
        return np.sign(np.dot(self.segment(signal), self._r))
         
